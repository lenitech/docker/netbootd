package cmd

import (
	"net"
	"os"
	"os/signal"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/lenitech/docker/netbootd/config"
	"gitlab.com/lenitech/docker/netbootd/tftpd"
)

var (
	tftpdListenAddress string
	tftpdListenPort    int
	tftpdApiUrl        string
	tftpdApiToken      string
)

func init() {
	tftpdCmd.Flags().StringVarP(&tftpdListenAddress, "listen-address", "l", "127.0.0.1", "IP address to listen on")
	tftpdCmd.Flags().IntVarP(&tftpdListenPort, "listen-port", "p", 6969, "UDP port to listen on")
	tftpdCmd.Flags().StringVarP(&tftpdApiUrl, "api-url", "a", "http://127.0.0.1:8080", "API url for fetching manifest")
	tftpdCmd.Flags().StringVarP(&tftpdApiToken, "api-token", "t", "", "Token for API auth")
	rootCmd.AddCommand(tftpdCmd)
}

var tftpdCmd = &cobra.Command{
	Use: "tftpd",
	PreRun: func(cmd *cobra.Command, args []string) {

		viper.BindPFlag("listen-address", cmd.Flags().Lookup("listen-address"))
		viper.BindPFlag("listen-port", cmd.Flags().Lookup("listen-port"))
		viper.BindPFlag("api-url", cmd.Flags().Lookup("api-url"))
		viper.BindPFlag("api-token", cmd.Flags().Lookup("api-token"))
	},
	Run: func(cmd *cobra.Command, args []string) {
		// configure logging
		config.InitZeroLog()
		if viper.GetBool("trace") {
			zerolog.SetGlobalLevel(zerolog.TraceLevel)
		} else if viper.GetBool("debug") {
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		} else {
			zerolog.SetGlobalLevel(zerolog.InfoLevel)
		}

		// TFTP
		tftpServer, err := tftpd.NewServer(viper.GetString("api-url"), viper.GetString("api-token"))
		if err != nil {
			log.Fatal().Err(err)
		}
		connTftp, err := net.ListenUDP("udp", &net.UDPAddr{
			IP:   net.ParseIP(viper.GetString("listen-address")),
			Port: viper.GetInt("listen-port"),
		})
		if err != nil {
			log.Fatal().Err(err)
		}
		go tftpServer.Serve(connTftp)

		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, os.Interrupt)
		<-sigs
	},
}
