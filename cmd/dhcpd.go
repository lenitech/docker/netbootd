package cmd

import (
	"os"
	"os/signal"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/lenitech/docker/netbootd/config"
	"gitlab.com/lenitech/docker/netbootd/dhcpd"
)

var (
	dhcpdListenAddress string
	dhcpdListenPort    int
	ifname             string
	dhcpdApiUrl        string
	dhcpdApiToken      string
)

func init() {
	dhcpdCmd.Flags().StringVarP(&dhcpdListenAddress, "listen-address", "l", "", "IP address to listen on")
	dhcpdCmd.Flags().IntVarP(&dhcpdListenPort, "listen-port", "p", 6767, "UDP port to listen on")
	dhcpdCmd.Flags().StringVarP(&ifname, "interface", "i", "", "interface to listen on, e.g. eth0 (DHCP)")
	dhcpdCmd.Flags().StringVarP(&dhcpdApiUrl, "api-url", "a", "http://127.0.0.1:8080", "API url for fetching manifest")
	dhcpdCmd.Flags().StringVarP(&dhcpdApiToken, "api-token", "t", "", "Token for API auth")
	rootCmd.AddCommand(dhcpdCmd)
}

var dhcpdCmd = &cobra.Command{
	Use: "dhcpd",
	PreRun: func(cmd *cobra.Command, args []string) {
		viper.BindPFlag("listen-address", cmd.Flags().Lookup("listen-address"))
		viper.BindPFlag("listen-port", cmd.Flags().Lookup("listen-port"))
		viper.BindPFlag("interface", cmd.Flags().Lookup("interface"))
		viper.BindPFlag("netboxUrl", cmd.Flags().Lookup("netbox-url"))
		viper.BindPFlag("netboxApiKey", cmd.Flags().Lookup("netbox-api-key"))
		viper.BindPFlag("kubeconfig", cmd.Flags().Lookup("kubeconfig"))
		viper.BindPFlag("api-url", cmd.Flags().Lookup("api-url"))
		viper.BindPFlag("api-token", cmd.Flags().Lookup("api-token"))

	},
	Run: func(cmd *cobra.Command, args []string) {
		// configure logging
		config.InitZeroLog()
		if viper.GetBool("trace") {
			zerolog.SetGlobalLevel(zerolog.TraceLevel)
		} else if viper.GetBool("debug") {
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		} else {
			zerolog.SetGlobalLevel(zerolog.InfoLevel)
		}

		// DHCP
		dhcpServer, err := dhcpd.NewServer(viper.GetString("listen-address"), viper.GetInt("listen-port"), viper.GetString("interface"), viper.GetString("api-url"), viper.GetString("api-token"))
		if err != nil {
			log.Fatal().Err(err)
		}
		go dhcpServer.Serve()

		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, os.Interrupt)
		<-sigs
	},
}
