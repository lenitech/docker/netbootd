package cmd

import (
	"net"
	"os"
	"os/signal"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/lenitech/docker/netbootd/api"
	"gitlab.com/lenitech/docker/netbootd/api/store"
	"gitlab.com/lenitech/docker/netbootd/config"
)

var (
	apiListenAddress   string
	apiListenPort      int
	apiTlsCert         string
	apiTlsKey          string
	netboxUrl          string
	netboxApiKey       string
	powerDNSUrl        string
	powerDNSApiKey     string
	kubeConfig         string
	dhcpdPrefix        string
	dhcpdRange         string
	dhcpdRouter        string
	dhcpdDNS           string
	dhcpdLeaseDuration string
)

func init() {
	apiCmd.Flags().StringVarP(&apiListenAddress, "listen-address", "l", "127.0.0.1", "IP address to listen on")
	apiCmd.Flags().IntVarP(&apiListenPort, "listen-port", "p", 8080, "TCP port to listen on")
	apiCmd.Flags().StringVar(&apiTlsCert, "api-tls-cert", "", "Path to TLS certificate API")
	apiCmd.Flags().StringVar(&apiTlsKey, "api-tls-key", "", "Path to TLS certificate for API")
	apiCmd.Flags().StringVarP(&dhcpdPrefix, "dhcpd-default-prefix", "", "", "Default prefix for dhcpd server")
	apiCmd.Flags().StringVarP(&dhcpdRange, "dhcpd-default-range", "", "", "Default range for dhcpd server")
	apiCmd.Flags().StringVarP(&dhcpdRouter, "dhcpd-default-router", "", "", "Default router for dhcpd server")
	apiCmd.Flags().StringVarP(&dhcpdDNS, "dhcpd-default-dns", "", "", "Default DNS server for dhcpd server")
	apiCmd.Flags().StringVarP(&dhcpdLeaseDuration, "dhcpd-default-lease-duration", "", "", "Default lease duration for dhcpd server")
	apiCmd.Flags().StringVarP(&netboxUrl, "netbox-url", "n", "", "load manifests from Netbox")
	apiCmd.Flags().StringVarP(&netboxApiKey, "netbox-api-key", "", "", "load manifests from Netbox")
	apiCmd.Flags().StringVarP(&powerDNSUrl, "powerdns-url", "", "", "update PowerDNS record from manifest")
	apiCmd.Flags().StringVarP(&powerDNSApiKey, "powerdns-api-key", "", "", "update PowerDNS record from manifest")
	apiCmd.Flags().StringVarP(&kubeConfig, "kubeconfig", "k", "", "load manifests from Kubernetes")
	rootCmd.AddCommand(apiCmd)
}

var apiCmd = &cobra.Command{
	Use: "api",
	PreRun: func(cmd *cobra.Command, args []string) {
		viper.BindPFlag("listen-address", cmd.Flags().Lookup("listen-address"))
		viper.BindPFlag("listen-port", cmd.Flags().Lookup("listen-port"))
		viper.BindPFlag("api.TLSCertificatePath", cmd.Flags().Lookup("api-tls-cert"))
		viper.BindPFlag("api.TLSPrivateKeyPath", cmd.Flags().Lookup("api-tls-key"))
		viper.BindPFlag("dhcpdDefaultPrefix", cmd.Flags().Lookup("dhcpd-default-prefix"))
		viper.BindPFlag("dhcpdDefaultRange", cmd.Flags().Lookup("dhcpd-default-range"))
		viper.BindPFlag("dhcpdDefaultRouter", cmd.Flags().Lookup("dhcpd-default-router"))
		viper.BindPFlag("dhcpdDefaultDNS", cmd.Flags().Lookup("dhcpd-default-dns"))
		viper.BindPFlag("dhcpdDefaultLeaseDuration", cmd.Flags().Lookup("dhcpd-default-lease-duration"))
		viper.BindPFlag("netboxUrl", cmd.Flags().Lookup("netbox-url"))
		viper.BindPFlag("netboxApiKey", cmd.Flags().Lookup("netbox-api-key"))
		viper.BindPFlag("powerDNSUrl", cmd.Flags().Lookup("powerdns-url"))
		viper.BindPFlag("powerDNSApiKey", cmd.Flags().Lookup("powerdns-api-key"))
		viper.BindPFlag("kubeconfig", cmd.Flags().Lookup("kubeconfig"))
	},
	Run: func(cmd *cobra.Command, args []string) {
		// configure logging
		config.InitZeroLog()
		if viper.GetBool("trace") {
			zerolog.SetGlobalLevel(zerolog.TraceLevel)
		} else if viper.GetBool("debug") {
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		} else {
			zerolog.SetGlobalLevel(zerolog.InfoLevel)
		}

		if viper.GetString("netboxUrl") != "" && viper.GetString("netboxApiKey") != "" {
			log.Info().Str("url", viper.GetString("netboxUrl")).Msg("Add support for Netbox store")
			store.InitNetbox(viper.GetString("netboxUrl"), viper.GetString("netboxApiKey"))
		}
		if viper.GetString("kubeconfig") != "" || (os.Getenv("KUBERNETES_SERVICE_HOST") != "" && os.Getenv("KUBERNETES_SERVICE_PORT") != "") {
			log.Info().Msg("Add support for Kubernetes store")
			err := store.InitKubernetes(viper.GetString("kubeconfig"))
			if err != nil {
				log.Fatal().Err(err)
			}
		} else {
			store.InitMemory()
		}

		if viper.GetString("powerDNSUrl") != "" && viper.GetString("powerDNSApiKey") != "" {
			log.Info().Str("url", viper.GetString("powerDNSUrl")).Msg("Add support for PowerDNS plugin")
			err := store.InitPowerDNS(viper.GetString("powerDNSUrl"), viper.GetString("powerDNSApiKey"))
			if err != nil {
				log.Fatal().Err(err)
			}
		}

		if viper.GetString("dhcpdDefaultPrefix") != "" {
			err := store.DefaultPrefix(viper.GetString("dhcpdDefaultPrefix"))
			if err != nil {
				log.Fatal().Err(err)
			}
		}
		if viper.GetString("dhcpdDefaultRange") != "" {
			err := store.DefaultRange(viper.GetString("dhcpdDefaultRange"))
			if err != nil {
				log.Fatal().Err(err)
			}
		}
		if viper.GetString("dhcpdDefaultRouter") != "" {
			err := store.DefaultRouter(viper.GetString("dhcpdDefaultRouter"))
			if err != nil {
				log.Fatal().Err(err)
			}
		}
		if viper.GetString("dhcpdDefaultDNS") != "" {
			err := store.DefaultDNS(viper.GetString("dhcpdDefaultDNS"))
			if err != nil {
				log.Fatal().Err(err)
			}
		}
		if viper.GetString("dhcpdDefaultLeaseDuration") != "" {
			err := store.DefaultLeaseDuration(viper.GetString("dhcpdDefaultLeaseDuration"))
			if err != nil {
				log.Fatal().Err(err)
			}
		}

		go func() {
			for {
				time.Sleep(5 * time.Second)
				for _, m := range store.GetExpired() {
					log.Info().
						Interface("manifest", m).
						Msg("manifest lease has expired, forget it")
					err := store.DeleteManifest(m)
					if err != nil {
						log.Error().
							Err(err).
							Msg("failed to forget manifest")
						continue
					}
				}
			}
		}()

		// HTTP API service
		apiServer, err := api.NewServer(viper.GetString("api.authorization"))
		if err != nil {
			log.Fatal().Err(err)
		}
		connApi, err := net.ListenTCP("tcp", &net.TCPAddr{
			IP:   net.ParseIP(viper.GetString("listen-address")),
			Port: viper.GetInt("listen-port"), // HTTP
		})
		if err != nil {
			log.Fatal().Err(err)
		}
		if viper.GetString("api.TLSCertificatePath") != "" && viper.GetString("api.TLSPrivateKeyPath") != "" {
			log.Info().Interface("api", connApi.Addr()).Msg("HTTP API listening with TLS...")
			go func() {
				err := apiServer.ServeTLS(connApi, viper.GetString("api.TLSCertificatePath"), viper.GetString("api.TLSPrivateKeyPath"))
				log.Error().Err(err).Msg("Error initializing TLS HTTP API listener!")
			}()
		} else {
			go apiServer.Serve(connApi)
			log.Info().Interface("api", connApi.Addr()).Msg("HTTP API listening...")
			go func() {
				err := apiServer.Serve(connApi)
				log.Error().Err(err).Msg("Error initializing HTTP API listener!")
			}()
		}
		if !viper.IsSet("api.authorization") {
			log.Warn().Interface("api", connApi.Addr()).Msg("API is running without authentication, set Authorization in config!")
		}

		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, os.Interrupt)
		<-sigs
	},
}
