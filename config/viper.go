package config

import (
	"github.com/spf13/viper"
	"strings"
)

var config Config

// https://github.com/spf13/viper
func InitConfig() {
	viper.SetConfigName("netbootd")
	viper.SetConfigType("yaml")

	viper.SetDefault("store.path", "/var/lib/netbootd")

	viper.SetEnvPrefix("netbootd")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}

// Read (or re-read) the config from external source
func Read() error {
	// https://github.com/spf13/viper#unmarshaling to struct
	return viper.Unmarshal(&config)
}

// Get copy of running config
func GetConfig() Config {
	return config
}
