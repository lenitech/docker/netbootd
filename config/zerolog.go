package config

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var (
	zerologInitDone bool // to prevent zerolog to be initialized twice in specific situations (like parsing error of viper configuration file)
)

func init() {
	// UNIX Time is faster and smaller than most timestamps
	// If you set zerolog.TimeFieldFormat to an empty string,
	// logs will write with UNIX time
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
}

func InitZeroLog() {
	if !zerologInitDone {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		log.Debug().Msg("Enabled console writer")
		zerologInitDone = true
	}
}
