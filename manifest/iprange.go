package manifest

import (
	"encoding/binary"
	"net"
	"strings"
)

type IPRange struct {
	Start net.IP
	End   net.IP
	IPs   []net.IP
}

func ParseIPRange(s string) *IPRange {
	ipr := IPRange{}
	ipRangeSlice := strings.Split(s, "-")
	ipr.Start = net.ParseIP(ipRangeSlice[0])
	ipr.End = net.ParseIP(ipRangeSlice[1])
	for i := binary.BigEndian.Uint32(ipr.Start.To4()); i <= binary.BigEndian.Uint32(ipr.End.To4()); i++ {
		ip := make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, i)
		ipr.IPs = append(ipr.IPs, ip)
	}
	return &ipr
}

func (r *IPRange) Contains(ip net.IP) bool {
	b := binary.BigEndian.Uint32(ip)
	if b >= binary.BigEndian.Uint32(r.Start.To4()) && b <= binary.BigEndian.Uint32(r.End.To4()) {
		return true
	} else {
		return false
	}
}

func (r *IPRange) GetAvailablesIPs(filterIPs []string) (ips []net.IP) {
	availableIps := make(map[string]bool)
	for _, ip := range r.IPs {
		availableIps[ip.String()] = true
		for _, fip := range filterIPs {
			if ip.String() == fip {
				availableIps[ip.String()] = false
			}
		}
	}
	for ip, available := range availableIps {
		if available {
			ips = append(ips, net.ParseIP(ip))
		}
	}
	return ips
}
