# netbootd

> :warning: This repository is not maintained anymore, if you want to assign IP by DHCP with Netbox as source of truth, you can report to [netbox-dhcpd](https://gitlab.com/lenitech/netbox-dhcpd).

Netbootd is a lightweight network boot server designed for automation. It connects to an IPAM (currently only Netbox) to assign IPs to hosts and automatically boots hosts to a configured system in the IPAM.

It is composed of three processes: an API server, a DHCP server, and a TFTP server.

## API

The API server is responsible for connecting to the IPAM, retrieving information from it, and exposing this information to DHCPD, TFTPD, and admin users.

### Usage

~~~
Usage:
  netbootd api [flags]

Flags:
      --api-tls-cert string                   Path to TLS certificate API
      --api-tls-key string                    Path to TLS certificate for API
      --dhcpd-default-dns string              Default DNS server for dhcpd server
      --dhcpd-default-lease-duration string   Default lease duration for dhcpd server
      --dhcpd-default-range string            Default range for dhcpd server
      --dhcpd-default-router string           Default router for dhcpd server
  -h, --help                                  help for api
  -k, --kubeconfig string                     load manifests from Kubernetes
  -l, --listen-address string                 IP address to listen on (default "127.0.0.1")
  -p, --listen-port int                       TCP port to listen on (default 8080)
      --netbox-api-key string                 load manifests from Netbox
  -n, --netbox-url string                     load manifests from Netbox
      --powerdns-api-key string               update PowerDNS record from manifest
      --powerdns-url string                   update PowerDNS record from manifest

Global Flags:
  -d, --debug                    enable debug logging
      --trace                    enable trace logging
~~~

## DHCPD

The DHCP daemon is responsible for IP assignment over the DHCP protocol. It retrieves the IP corresponding to a host based on the host's MAC address by making a request to the API server.

### Usage

~~~
Usage:
  netbootd dhcpd [flags]

Flags:
  -t, --api-token string        Token for API auth
  -a, --api-url string          API url for fetching manifest (default "http://127.0.0.1:8080")
  -h, --help                    help for dhcpd
  -i, --interface string        interface to listen on, e.g. eth0 (DHCP)
  -l, --listen-address string   IP address to listen on
  -p, --listen-port int         UDP port to listen on (default 6767)

Global Flags:
  -d, --debug                    enable debug logging
      --trace                    enable trace logging
~~~

## TFTPD

The TFTP daemon is responsible for sending boot files over the TFTP protocol to hosts. It knows which files it must send to a host based on the host's IP address by making a request to the API server.

It embeds some IPXE binaries to assist in the network booting of complex exploitation systems.

### Usage

~~~
Usage:
  netbootd tftpd [flags]

Flags:
  -t, --api-token string        Token for API auth
  -a, --api-url string          API url for fetching manifest (default "http://127.0.0.1:8080")
  -h, --help                    help for tftpd
  -l, --listen-address string   IP address to listen on (default "127.0.0.1")
  -p, --listen-port int         UDP port to listen on (default 6969)

Global Flags:
  -d, --debug                    enable debug logging
      --trace                    enable trace logging
~~~

## Author

This project is maintained by [Leni.tech](https://www.leni.tech/) and is a fork of an original project from [github.com/DSpeichert/netbootd](https://github.com/DSpeichert/netbootd).

## License

This project is Apache 2.0 Licensed. See [LICENSE]() for full details.
