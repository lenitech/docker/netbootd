package store

import (
	"bytes"
	"encoding/json"
	"fmt"
	transport "github.com/go-openapi/runtime/client"
	"github.com/insomniacslk/dhcp/dhcpv4"
	"github.com/netbox-community/go-netbox/v3/netbox/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client/dcim"
	"github.com/netbox-community/go-netbox/v3/netbox/client/ipam"
	"github.com/netbox-community/go-netbox/v3/netbox/models"
	"github.com/rs/zerolog/log"
	"gitlab.com/lenitech/docker/netbootd/manifest"
	"net"
	"strconv"
	"strings"
	"time"
)

var pageLimit = int64(500)
var yes = "true"

type netboxConfig struct {
	url    string
	client *client.NetBoxAPI
}

func InitNetbox(url string, apiKey string) error {
	store := Store{}
	store.logger = log.With().Str("module", "store").Str("store", "netbox").Logger()
	store.GetByID = getByIdFromNetbox
	store.GetByIP = getByIPFromNetbox
	store.GetByMAC = getByMACFromNetbox
	store.GetAll = getAllFromNetbox
	store.GetUsedIPs = getUsedIPsFromNetbox
	store.Suspend = suspendFromNetbox
	store.Unsuspend = unsuspendFromNetbox
	t := transport.New(url, client.DefaultBasePath, []string{"https"})
	t.DefaultAuthentication = transport.APIKeyAuth(
		"Authorization",
		"header",
		fmt.Sprintf("Token %v", apiKey),
	)
	store.config = netboxConfig{
		url:    url,
		client: client.New(t, nil),
	}
	stores["netbox"] = &store
	return nil
}

func getByIdFromNetbox(id string) (m *manifest.Manifest) {
	m = nil
	s := stores["netbox"]
	config := s.config.(netboxConfig)
	nid, _ := strconv.ParseInt(id, 10, 64)
	req := dcim.NewDcimDevicesReadParams().
		WithID(nid)
	res, err := config.client.Dcim.DcimDevicesRead(req, nil)
	if err != nil {
		s.logger.Error().
			Str("id", id).
			Err(err).
			Msg("failed to get device by ID from Netbox")
		return nil
	}
	m = netboxParseDevice(res.Payload)
	return m
}

func getByIPFromNetbox(ip string) (m *manifest.Manifest) {
	m = nil
	s := stores["netbox"]
	config := s.config.(netboxConfig)
	req := ipam.NewIpamIPAddressesListParams().
		WithLimit(&pageLimit).
		WithAddress(&ip).
		WithAssignedToInterface(&yes)
	res, err := config.client.Ipam.IpamIPAddressesList(req, nil)
	if err != nil {
		s.logger.Error().
			Str("ip", ip).
			Err(err).
			Msg("failed to get IPs list from Netbox")
		return nil
	}
	for _, ipAddr := range res.Payload.Results {
		i := ipAddr.AssignedObject.(map[string]interface{})
		if i["device"] != nil {
			device := i["device"].(map[string]interface{})
			m = getByIdFromNetbox(fmt.Sprintf("%s", device["id"]))
			if m != nil {
				break
			}
		}
	}
	return m
}

func getByMACFromNetbox(mac string) (m *manifest.Manifest) {
	m = nil
	s := stores["netbox"]
	config := s.config.(netboxConfig)
	req := dcim.NewDcimDevicesListParams().
		WithLimit(&pageLimit).
		WithMacAddress(&mac)
	res, err := config.client.Dcim.DcimDevicesList(req, nil)
	if err != nil {
		s.logger.Error().
			Err(err).
			Msg("failed get devices with mac from Netbox")
		return nil
	}
	for _, device := range res.Payload.Results {
		m = netboxParseDevice(device)
		if m != nil {
			break
		}
	}
	return m
}

func getAllFromNetbox() (manifests map[string]*manifest.Manifest) {
	manifests = make(map[string]*manifest.Manifest)
	s := stores["netbox"]
	config := s.config.(netboxConfig)
	req := dcim.NewDcimDevicesListParams().
		WithLimit(&pageLimit).
		WithHasPrimaryIP(&yes).
		WithInterfaces(&yes)
	res, err := config.client.Dcim.DcimDevicesList(req, nil)
	if err != nil {
		s.logger.Error().
			Err(err).
			Msg("failed to list devices from Netbox")
		return manifests
	}
	for _, device := range res.Payload.Results {
		m := netboxParseDevice(device)
		if m != nil {
			manifests[strconv.Itoa(int(device.ID))] = m
		}
	}
	return manifests
}

func getUsedIPsFromNetbox() (ips []string) {
	s := stores["netbox"]
	config := s.config.(netboxConfig)
	req := dcim.NewDcimDevicesListParams().
		WithLimit(&pageLimit).
		WithHasPrimaryIP(&yes).
		WithInterfaces(&yes)
	res, err := config.client.Dcim.DcimDevicesList(req, nil)
	if err != nil {
		s.logger.Error().
			Err(err).
			Msg("failed to list devices from Netbox")
		return ips
	}
	for _, device := range res.Payload.Results {
		if device.PrimaryIp4 != nil {
			ip, _, err := net.ParseCIDR(*device.PrimaryIp4.Address)
			if err != nil {
				s.logger.Error().
					Err(err).
					Msg("failed to parse IP from Netbox device")
				continue
			}
			ips = append(ips, ip.String())
		}
	}
	return ips
}

func netboxParseDevice(device *models.DeviceWithConfigContext) *manifest.Manifest {
	s := stores["netbox"]
	config := s.config.(netboxConfig)
	id := strconv.Itoa(int(device.ID))
	m := manifest.Manifest{
		ID: id,
	}
	if device.PrimaryIp4 != nil {
		ip, ipnet, err := net.ParseCIDR(*device.PrimaryIp4.Address)
		if err != nil {
			s.logger.Error().
				Err(err).
				Msg("failed to parse IP from Netbox device")
			return nil
		}
		m.IPv4 = manifest.IPWithNet{
			IP:  ip,
			Net: *ipnet,
		}
	} else {
		return nil
	}
	m.Hostname = *device.Name
	if *device.Status.Value != "staged" {
		m.Suspended = true
	}
	req2 := dcim.NewDcimInterfacesListParams().
		WithDeviceID(&id)
	res2, err := config.client.Dcim.DcimInterfacesList(req2, nil)
	if err != nil {
		s.logger.Error().
			Err(err).
			Msg("cannot get Netbox interfaces list")
		return nil
	}
	for _, i := range res2.Payload.Results {
		if i.MacAddress != nil {
			hw, err := manifest.ParseMAC(*i.MacAddress)
			if err != nil {
				s.logger.Error().
					Err(err).
					Msg("cannot add device MAC from Netbox")
				return nil
			}
			m.MAC = append(m.MAC, hw)
		}
	}
	if device.Platform != nil {
		pid := strconv.Itoa(int(device.Platform.ID))
		req3 := dcim.NewDcimPlatformsListParams().
			WithID(&pid)
		res3, err := config.client.Dcim.DcimPlatformsList(req3, nil)
		if err != nil {
			s.logger.Error().
				Err(err).
				Msg("cannot get Netbox platform detail")
			return nil
		}
		for _, p := range res3.Payload.Results {
			for key, value := range p.CustomFields.(map[string]interface{}) {
				if value != nil {
					if key == "Ipxe" {
						m.BootFilename = "boot.ipxe"
						content := []byte(value.(string))
						content = bytes.Replace(content, []byte{13, 10}, []byte{10}, -1)
						m.Mounts = append(m.Mounts, manifest.Mount{
							Path:    "/boot.ipxe",
							Content: string(content),
						})
					} else if key == "Config" {
						content := []byte(value.(string))
						content = bytes.Replace(content, []byte{13, 10}, []byte{10}, -1)
						m.Mounts = append(m.Mounts, manifest.Mount{
							Path:    "/config.cfg",
							Content: string(content),
						})
					}
				}
			}
		}
	}
	if device.PrimaryIp4 != nil {
		req3 := ipam.NewIpamPrefixesListParams().
			WithContains(device.PrimaryIp4.Address)
		res3, err := config.client.Ipam.IpamPrefixesList(req3, nil)
		if err != nil {
			s.logger.Error().
				Err(err).
				Msg("cannot get Netbox prefix detail")
			return nil
		}
		for _, p := range res3.Payload.Results {
			for key, value := range p.CustomFields.(map[string]interface{}) {
				if value != nil {
					switch key {
					case "Gateway", "dhcp_routers":
						m.Router = append(m.Router, net.ParseIP(value.(string)))
					case "Nameserver", "dhcp_name_servers":
						for _, s := range strings.Split(value.(string), ",") {
							m.DNS = append(m.DNS, net.ParseIP(s))
						}
					case "NTP", "dhcp_ntp_servers":
						for _, s := range strings.Split(value.(string), ",") {
							m.NTP = append(m.NTP, net.ParseIP(s))
						}
					case "Domain", "dhcp_domain":
						m.Domain = value.(string)
					case "Routes", "dhcp_routes":
						for _, s := range strings.Split(value.(string), ",") {
							r := strings.Split(s, "-")
							_, dest, _ := net.ParseCIDR(r[0])
							router := net.ParseIP(r[1])
							route := dhcpv4.Route{
								Dest:   dest,
								Router: router,
							}
							m.Route = append(m.Route, &route)
						}
					case "Leaseduration", "dhcp_lease_ttl":
						m.LeaseDuration, err = time.ParseDuration(fmt.Sprintf("%ss", value.(json.Number)))
						if err != nil {
							s.logger.Error().
								Err(err).
								Msg("failed to parse lease duration from Netbox")
						}
					}
				}
			}
		}
	}
	return &m
}

func suspendFromNetbox(id string) (err error) {
	s := stores["netbox"]
	config := s.config.(netboxConfig)
	nid, _ := strconv.ParseInt(id, 10, 64)
	req := dcim.NewDcimDevicesReadParams().
		WithID(nid)
	res, err := config.client.Dcim.DcimDevicesRead(req, nil)
	if err != nil {
		s.logger.Error().
			Str("id", id).
			Err(err).
			Msg("failed to get device by ID from Netbox")
	} else {
		req := dcim.NewDcimDevicesPartialUpdateParams().
			WithID(nid).
			WithData(&models.WritableDeviceWithConfigContext{
				DeviceRole: &res.Payload.DeviceRole.ID,
				DeviceType: &res.Payload.DeviceType.ID,
				Site:       &res.Payload.Site.ID,
				Status:     "active",
			})
		_, err := config.client.Dcim.DcimDevicesPartialUpdate(req, nil)
		if err != nil {
			s.logger.Error().
				Err(err).
				Msg("cannot update Netbox device status")
		}
	}
	return err
}

func unsuspendFromNetbox(id string) (err error) {
	s := stores["netbox"]
	config := s.config.(netboxConfig)
	nid, _ := strconv.ParseInt(id, 10, 64)
	req := dcim.NewDcimDevicesReadParams().
		WithID(nid)
	res, err := config.client.Dcim.DcimDevicesRead(req, nil)
	if err != nil {
		s.logger.Error().
			Str("id", id).
			Err(err).
			Msg("failed to get device by ID from Netbox")
	} else {
		req := dcim.NewDcimDevicesPartialUpdateParams().
			WithID(nid).
			WithData(&models.WritableDeviceWithConfigContext{
				DeviceRole: &res.Payload.DeviceRole.ID,
				DeviceType: &res.Payload.DeviceType.ID,
				Site:       &res.Payload.Site.ID,
				Status:     "staged",
			})
		_, err := config.client.Dcim.DcimDevicesPartialUpdate(req, nil)
		if err != nil {
			s.logger.Error().
				Err(err).
				Msg("cannot update Netbox device status")
		}
	}
	return err
}
