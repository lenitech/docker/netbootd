package store

import (
	mfest "gitlab.com/lenitech/docker/netbootd/manifest"
	"net"
	"strings"
	"time"
)

func DefaultPrefix(r string) (err error) {
	_, ipnet, err := net.ParseCIDR(r)
	defaultPrefix = ipnet
	if err != nil {
		logger.Error().
			Interface("ipprefix", defaultPrefix).
			Msg("failed to parse default ip prefix")
		return err
	}
	return nil
}

func DefaultRange(r string) (err error) {
	defaultRange = mfest.ParseIPRange(r)
	if defaultRange == nil {
		logger.Error().
			Interface("iprange", defaultRange).
			Msg("failed to parse default ip range")
		return err
	}
	return nil
}

func DefaultRouter(r string) (err error) {
	defaultRouter = net.ParseIP(r)
	if defaultRouter == nil {
		logger.Error().
			Interface("ip", defaultRouter).
			Msg("failed to parse default router ip")
		return err
	}
	return nil
}

func DefaultDNS(r string) (err error) {
	for _, s := range strings.Split(r, ",") {
		defaultDNS = append(defaultDNS, net.ParseIP(s))
	}
	if defaultDNS == nil {
		logger.Error().
			Interface("ip", defaultDNS).
			Msg("failed to parse default DNS ip")
		return err
	}
	return nil
}

func DefaultLeaseDuration(r string) (err error) {
	defaultLeaseDuration, err = time.ParseDuration(r)
	if err != nil {
		logger.Error().
			Err(err).
			Msg("failed to parse default lease duration")
		return err
	}
	return nil
}
