package store

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/lenitech/docker/netbootd/manifest"
	"net"
	"strings"
	"time"
)

var (
	stores               map[string]*Store
	logger               zerolog.Logger
	defaultPrefix        *net.IPNet
	defaultRange         *manifest.IPRange
	defaultRouter        net.IP
	defaultDNS           []net.IP
	defaultLeaseDuration time.Duration
)

type Store struct {
	config         interface{}
	logger         zerolog.Logger
	PutManifest    func(manifest.Manifest) error
	DeleteManifest func(*manifest.Manifest) error
	GetByID        func(string) *manifest.Manifest
	GetByIP        func(string) *manifest.Manifest
	GetByMAC       func(string) *manifest.Manifest
	GetAll         func() map[string]*manifest.Manifest
	GetExpired     func() map[string]*manifest.Manifest
	GetUsedIPs     func() []string
	UpdateLease    func(string) error
	Suspend        func(string) error
	Unsuspend      func(string) error
}

func init() {
	stores = make(map[string]*Store)
	logger = log.With().Str("module", "store").Logger()
}

func PutManifest(m manifest.Manifest) (err error) {
	err = nil
	for _, store := range stores {
		if store.PutManifest != nil {
			err := store.PutManifest(m)
			if err != nil {
				continue
			}
		}
	}
	return err
}

func DeleteManifest(m *manifest.Manifest) (err error) {
	err = nil
	for _, store := range stores {
		if store.DeleteManifest != nil {
			m := store.DeleteManifest(m)
			if m != nil {
				continue
			}
		}
	}
	return err
}

func GetByID(id string) (m *manifest.Manifest) {
	m = nil
	for _, store := range stores {
		if store.GetByID != nil {
			m = store.GetByID(id)
			if m != nil {
				break
			}
		}
	}
	return m
}

func GetByIP(ip string) (m *manifest.Manifest) {
	m = nil
	for _, store := range stores {
		if store.GetByIP != nil {
			m = store.GetByIP(ip)
			if m != nil {
				break
			}
		}
	}
	return m
}

func GetByMAC(mac string) (m *manifest.Manifest) {
	m = nil
	for _, store := range stores {
		if store.GetByMAC != nil {
			m = store.GetByMAC(mac)
			if m != nil {
				break
			}
		}
	}
	return m
}

func GetAll() (manifests map[string]*manifest.Manifest) {
	manifests = make(map[string]*manifest.Manifest)
	for _, store := range stores {
		if store.GetAll != nil {
			for id, m := range store.GetAll() {
				if m != nil {
					manifests[id] = m
				}
			}
		}
	}
	return manifests
}

func GetExpired() (manifests map[string]*manifest.Manifest) {
	manifests = make(map[string]*manifest.Manifest)
	for _, store := range stores {
		if store.GetExpired != nil {
			for id, m := range store.GetExpired() {
				if m != nil {
					manifests[id] = m
				}
			}
		}
	}
	return manifests
}

func GetUsedIPs() (ips []string) {
	for _, store := range stores {
		if store.GetUsedIPs != nil {
			ips = append(ips, store.GetUsedIPs()...)
		}
	}
	return ips
}

func GetDynamic(mac string) (m *manifest.Manifest, err error) {
	m = nil
	err = nil
	hw, _ := manifest.ParseMAC(mac)
	if defaultRange != nil {
		ips := defaultRange.GetAvailablesIPs(GetUsedIPs())
		if len(ips) > 0 {
			m = &manifest.Manifest{
				Suspended: true,
			}
			m.MAC = append(m.MAC, hw)
			m.ID = strings.Replace(m.MAC[0].String(), ":", "-", -1)
			m.IPv4 = manifest.IPWithNet{
				IP:  ips[0],
				Net: *defaultPrefix,
			}
			if defaultRouter != nil && len(m.Router) == 0 {
				m.Router = append(m.Router, defaultRouter)
			}
			if defaultDNS != nil && len(m.DNS) == 0 {
				m.DNS = defaultDNS
			}
			if defaultLeaseDuration != 0 && m.LeaseDuration == 0 {
				m.LeaseDuration = defaultLeaseDuration
			}
			err = PutManifest(*m)
		} else {
			err = fmt.Errorf("no availables IPs in range")
		}
	} else {
		err = fmt.Errorf("no dynamic range configured")
	}
	return m, err
}

func UpdateLease(id string) (err error) {
	err = nil
	for _, store := range stores {
		if store.UpdateLease != nil {
			err = store.UpdateLease(id)
		}
	}
	return err
}

func Suspend(id string) (err error) {
	for _, store := range stores {
		if store.Suspend != nil {
			err = store.Suspend(id)
		}
	}
	return err
}

func Unsuspend(id string) (err error) {
	for _, store := range stores {
		if store.Unsuspend != nil {
			err = store.Unsuspend(id)
		}
	}
	return err
}
