package store

import (
	"context"
	"fmt"
	"github.com/joeig/go-powerdns/v3"
	"github.com/rs/zerolog/log"
	mfest "gitlab.com/lenitech/docker/netbootd/manifest"
	"net"
	"strings"
)

// https://gist.github.com/trajber/7cb6abd66d39662526df
const hexDigit = "0123456789abcdef"

// https://gist.github.com/trajber/7cb6abd66d39662526df
func uitoa(val uint) string {
	if val == 0 { // avoid string allocation
		return "0"
	}
	var buf [20]byte // big enough for 64bit value base 10
	i := len(buf) - 1
	for val >= 10 {
		q := val / 10
		buf[i] = byte('0' + val - q*10)
		i--
		val = q
	}
	// val < 10
	buf[i] = byte('0' + val)
	return string(buf[i:])
}

// https://gist.github.com/trajber/7cb6abd66d39662526df
func ReverseAddr(addr string) (arpa string, err error) {
	ip := net.ParseIP(addr)
	if ip == nil {
		return "", &net.DNSError{Err: "unrecognized address", Name: addr}
	}
	if ip.To4() != nil {
		return uitoa(uint(ip[15])) + "." + uitoa(uint(ip[14])) + "." + uitoa(uint(ip[13])) + "." + uitoa(uint(ip[12])) + ".in-addr.arpa.", nil
	}
	// Must be IPv6
	buf := make([]byte, 0, len(ip)*4+len("ip6.arpa."))
	// Add it, in reverse, to the buffer
	for i := len(ip) - 1; i >= 0; i-- {
		v := ip[i]
		buf = append(buf, hexDigit[v&0xF])
		buf = append(buf, '.')
		buf = append(buf, hexDigit[v>>4])
		buf = append(buf, '.')
	}
	// Append "ip6.arpa." and return (buf already has the final .)
	buf = append(buf, "ip6.arpa."...)
	return string(buf), nil
}

type powerDNSConfig struct {
	url    string
	client *powerdns.Client
}

func InitPowerDNS(url string, apiKey string) error {
	store := Store{}
	store.logger = log.With().Str("module", "store").Str("store", "powerdns").Logger()
	store.UpdateLease = updateLeaseInPowerDNS
	store.DeleteManifest = deleteManifestInPowerDNS
	store.config = powerDNSConfig{
		url:    url,
		client: powerdns.NewClient(url, "localhost", map[string]string{"X-API-Key": apiKey}, nil),
	}
	stores["powerdns"] = &store
	return nil
}

func updateLeaseInPowerDNS(id string) error {
	s := stores["powerdns"]
	config := s.config.(powerDNSConfig)
	manifest := GetByID(id)
	if manifest.Hostname != "" && manifest.Domain != "" {
		ctx := context.Background()
		pdns := config.client
		zones, err := pdns.Zones.List(ctx)
		if err != nil {
			s.logger.Error().
				Err(err).
				Msg("cannot get PowerDNS zones list")
		}
		aIP, err := ReverseAddr(manifest.IPv4.IP.String())
		aDomain := strings.Join(strings.Split(aIP, ".")[1:], ".")
		if err != nil {
			s.logger.Warn().
				Err(err).
				Msg("cannot get reverse record of manifest IPv4")
		}
		for _, zone := range zones {
			if fmt.Sprintf("%s.", manifest.Domain) == *zone.Name {
				err := pdns.Records.Change(ctx, manifest.Domain, fmt.Sprintf("%s.%s", manifest.Hostname, manifest.Domain), powerdns.RRTypeA, 300, []string{manifest.IPv4.IP.String()})
				if err != nil {
					s.logger.Error().
						Err(err).
						Msg("cannot update PowerDNS A record")
				}
			} else if aDomain == *zone.Name {
				err := pdns.Records.Change(ctx, aDomain, aIP, powerdns.RRTypePTR, 300, []string{fmt.Sprintf("%s.%s.", manifest.Hostname, manifest.Domain)})
				if err != nil {
					s.logger.Error().
						Err(err).
						Msg("cannot update PowerDNS PTR record")
				}
			}
		}
	}
	return nil
}

func deleteManifestInPowerDNS(m *mfest.Manifest) error {
	s := stores["powerdns"]
	config := s.config.(powerDNSConfig)
	if m.Hostname != "" && m.Domain != "" {
		ctx := context.Background()
		pdns := config.client
		zones, err := pdns.Zones.List(ctx)
		if err != nil {
			s.logger.Error().
				Err(err).
				Msg("cannot get PowerDNS zones list")
		}
		aIP, err := ReverseAddr(m.IPv4.IP.String())
		aDomain := strings.Join(strings.Split(aIP, ".")[1:], ".")
		if err != nil {
			s.logger.Warn().
				Err(err).
				Msg("cannot get reverse record of manifest IPv4")
		}
		for _, zone := range zones {
			if fmt.Sprintf("%s.", m.Domain) == *zone.Name {
				err := pdns.Records.Delete(ctx, m.Domain, fmt.Sprintf("%s.%s", m.Hostname, m.Domain), powerdns.RRTypeA)
				if err != nil {
					s.logger.Error().
						Err(err).
						Msg("cannot delete PowerDNS A record")
				}
			} else if aDomain == *zone.Name {
				err := pdns.Records.Delete(ctx, aDomain, aIP, powerdns.RRTypePTR)
				if err != nil {
					s.logger.Error().
						Err(err).
						Msg("cannot delete PowerDNS PTR record")
				}
			}
		}
	}
	return nil
}
