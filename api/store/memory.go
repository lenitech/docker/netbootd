package store

import (
	"errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/lenitech/docker/netbootd/manifest"
	"sync"
	"time"
)

type memoryConfig struct {
	// mapping Manifest ID to Manifest
	manifests map[string]*manifest.Manifest

	// mapping IP Address to Manifest
	// IP is normalized string(ip.To16)
	ip map[string]*manifest.Manifest

	// mapping Mac Address to Manifest
	mac map[string]*manifest.Manifest

	mutex sync.RWMutex
}

func InitMemory() error {
	store := Store{}
	store.logger = log.With().Str("module", "store").Str("store", "memory").Logger()
	store.config = memoryConfig{
		manifests: make(map[string]*manifest.Manifest),
		ip:        make(map[string]*manifest.Manifest),
		mac:       make(map[string]*manifest.Manifest),
	}
	store.PutManifest = putManifestInMemory
	store.DeleteManifest = deleteManifestInMemory
	store.GetByID = func(id string) *manifest.Manifest {
		config := stores["memory"].config.(memoryConfig)
		config.mutex.RLock()
		defer config.mutex.RUnlock()
		return config.manifests[id]
	}
	store.GetByIP = func(ip string) *manifest.Manifest {
		config := stores["memory"].config.(memoryConfig)
		config.mutex.RLock()
		defer config.mutex.RUnlock()

		return config.ip[ip]
	}
	store.GetByMAC = func(mac string) *manifest.Manifest {
		config := stores["memory"].config.(memoryConfig)
		config.mutex.RLock()
		defer config.mutex.RUnlock()

		return config.mac[mac]
	}
	store.GetAll = func() map[string]*manifest.Manifest {
		config := stores["memory"].config.(memoryConfig)
		config.mutex.RLock()
		defer config.mutex.RUnlock()

		return config.manifests
	}
	store.GetUsedIPs = func() (ips []string) {
		config := stores["memory"].config.(memoryConfig)
		config.mutex.RLock()
		defer config.mutex.RUnlock()

		for ip := range config.ip {
			ips = append(ips, ip)
		}
		return ips
	}
	store.GetExpired = func() (manifests map[string]*manifest.Manifest) {
		manifests = make(map[string]*manifest.Manifest)
		config := stores["memory"].config.(memoryConfig)
		config.mutex.RLock()
		defer config.mutex.RUnlock()

		for id, m := range config.manifests {
			if m.LeaseDuration != 0 && !m.LastLease.IsZero() {
				if time.Since(m.LastLease) >= m.LeaseDuration {
					manifests[id] = m
				}
			}
		}
		return manifests
	}
	stores["memory"] = &store
	return nil
}

func putManifestInMemory(m manifest.Manifest) error {
	s := stores["memory"].config.(memoryConfig)
	if m.IPv4.IP == nil {
		return errors.New("no IPv4 address provided")
	}

	if m.ID == "" {
		return errors.New("ID cannot be null")
	}

	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.manifests[m.ID] = &m
	s.ip[string(m.IPv4.IP.String())] = &m
	for _, mac := range m.MAC {
		s.mac[mac.String()] = &m
	}

	return nil
}

func deleteManifestInMemory(m *manifest.Manifest) error {
	s := stores["memory"].config.(memoryConfig)
	s.mutex.RLock()
	m, ok := s.manifests[m.ID]
	s.mutex.RUnlock()
	if !ok {
		return nil
	}

	s.mutex.Lock()
	defer s.mutex.Unlock()

	delete(s.manifests, m.ID)
	delete(s.ip, string(m.IPv4.IP.To16()))
	for _, mac := range m.MAC {
		delete(s.mac, mac.String())
	}

	return nil
}
