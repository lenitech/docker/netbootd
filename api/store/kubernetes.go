package store

import (
	"context"
	"github.com/rs/zerolog/log"
	"gitlab.com/lenitech/docker/netbootd/manifest"
	"gopkg.in/yaml.v3"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"time"
)

type kubernetesConfig struct {
	kubeconfig *rest.Config
	client     *kubernetes.Clientset
}

func InitKubernetes(kubepath string) (err error) {
	store := Store{}
	store.logger = log.With().Str("module", "store").Str("store", "kubernetes").Logger()
	var kubeconfig *rest.Config
	if kubepath != "" {
		kubeconfig, err = clientcmd.BuildConfigFromFlags("", kubepath)
		if err != nil {
			return err
		}
	} else {
		kubeconfig, err = rest.InClusterConfig()
		if err != nil {
			return err
		}
	}
	var client *kubernetes.Clientset
	client, err = kubernetes.NewForConfig(kubeconfig)
	if err != nil {
		return err
	}
	store.config = kubernetesConfig{
		kubeconfig: kubeconfig,
		client:     client,
	}
	store.PutManifest = putManifestInKubernetes
	store.DeleteManifest = deleteManifestInKubernetes
	store.UpdateLease = updateLeaseInKubernetes
	store.GetAll = getAllFromKubernetes
	store.GetByID = func(id string) *manifest.Manifest {
		manifests := getAllFromKubernetes()
		if m, ok := manifests[id]; ok {
			return m
		}
		return nil
	}
	store.GetByIP = func(ip string) *manifest.Manifest {
		for _, m := range getAllFromKubernetes() {
			if ip == m.IPv4.IP.String() {
				return m
			}
		}
		return nil
	}
	store.GetByMAC = func(mac string) *manifest.Manifest {
		for _, m := range getAllFromKubernetes() {
			for _, h := range m.MAC {
				if mac == h.String() {
					return m
				}
			}
		}
		return nil
	}
	store.GetUsedIPs = func() (ips []string) {
		for _, m := range getAllFromKubernetes() {
			ips = append(ips, m.IPv4.IP.String())
		}
		return ips
	}
	store.GetExpired = func() (manifests map[string]*manifest.Manifest) {
		manifests = make(map[string]*manifest.Manifest)
		for id, m := range getAllFromKubernetes() {
			if m.LeaseDuration != 0 && !m.LastLease.IsZero() {
				if time.Since(m.LastLease) >= m.LeaseDuration {
					manifests[id] = m
				}
			}
		}
		return manifests
	}
	stores["kubernetes"] = &store
	if stores["memory"] != nil {
		delete(stores, "memory")
	}
	return nil
}

func getAllFromKubernetes() (manifests map[string]*manifest.Manifest) {
	manifests = make(map[string]*manifest.Manifest)
	config := stores["kubernetes"].config.(kubernetesConfig)
	var cm *corev1.ConfigMap
	cm, _ = config.client.CoreV1().ConfigMaps("kube-system").Get(context.TODO(), "netbootd-manifests", metav1.GetOptions{})
	for _, mm := range cm.Data {
		m := manifest.Manifest{}
		yaml.Unmarshal([]byte(mm), &m)
		manifests[m.ID] = &m
	}
	return manifests
}

func putManifestInKubernetes(m manifest.Manifest) (err error) {
	config := stores["kubernetes"].config.(kubernetesConfig)
	var cm *corev1.ConfigMap
	cm, err = config.client.CoreV1().ConfigMaps("kube-system").Get(context.TODO(), "netbootd-manifests", metav1.GetOptions{})
	if errors.IsNotFound(err) {
		cm = &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name: "netbootd-manifests",
			},
			Data: make(map[string]string),
		}
		_, err := config.client.CoreV1().ConfigMaps("kube-system").Create(context.TODO(), cm, metav1.CreateOptions{})
		if err != nil {
			return nil
		}
	} else if err != nil {
		return err
	}
	var b []byte
	b, err = yaml.Marshal(m)
	if err != nil {
		return err
	}
	if cm.Data == nil {
		cm.Data = make(map[string]string)
	}
	cm.Data[m.ID] = string(b)
	_, err = config.client.CoreV1().ConfigMaps("kube-system").Update(context.TODO(), cm, metav1.UpdateOptions{})
	if err != nil {
		return err
	}
	return nil
}

func deleteManifestInKubernetes(m *manifest.Manifest) (err error) {
	config := stores["kubernetes"].config.(kubernetesConfig)
	var cm *corev1.ConfigMap
	cm, err = config.client.CoreV1().ConfigMaps("kube-system").Get(context.TODO(), "netbootd-manifests", metav1.GetOptions{})
	if err != nil {
		return err
	}
	delete(cm.Data, m.ID)
	_, err = config.client.CoreV1().ConfigMaps("kube-system").Update(context.TODO(), cm, metav1.UpdateOptions{})
	if err != nil {
		return err
	}
	return nil
}

func updateLeaseInKubernetes(id string) (err error) {
	m := stores["kubernetes"].GetByID(id)
	if m != nil {
		m.LastLease = time.Now()
		err = stores["kubernetes"].PutManifest(*m)
	}
	return err
}
