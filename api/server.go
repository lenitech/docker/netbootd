package api

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"fmt"
	"github.com/Masterminds/sprig/v3"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/lenitech/docker/netbootd/api/store"
	"gitlab.com/lenitech/docker/netbootd/manifest"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"
	"time"
)

type Server struct {
	router     *mux.Router
	httpServer *http.Server

	logger zerolog.Logger
}

// NewServer set up HTTP API server instance
// If authorization is passed, requires privileged operation callers to present Authorization header with this content.
func NewServer(authorization string) (server *Server, err error) {
	r := mux.NewRouter()

	server = &Server{
		router: r,
		httpServer: &http.Server{
			Handler:        r,
			WriteTimeout:   10 * time.Second,
			ReadTimeout:    10 * time.Second,
			MaxHeaderBytes: 1 << 20,
			IdleTimeout:    10 * time.Second,
		},
		logger: log.With().Str("service", "api").Logger(),
	}

	// custom server header
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Server", "netbootd")
			next.ServeHTTP(w, r)
		})
	})

	// custom proxy headers
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			xForwardedFor := r.Header.Get("X-Forwarded-For")
			if queryFirst(r, "spoof") != "" {
				if authorization != r.Header.Get("Authorization") {
					http.Error(w, "Forbidden", http.StatusForbidden)
					return
				}
				r.RemoteAddr = net.ParseIP(queryFirst(r, "spoof")).String()
			} else if xForwardedFor != "" {
				ips := strings.Split(xForwardedFor, ",")
				clientIP := strings.TrimSpace(ips[0])
				r.RemoteAddr = net.ParseIP(clientIP).String()
			} else {
				host, _, _ := net.SplitHostPort(r.RemoteAddr)
				r.RemoteAddr = net.ParseIP(host).String()
			}
			next.ServeHTTP(w, r)
		})
	})

	// custom logging middleware
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			next.ServeHTTP(w, r)
			stop := time.Now()
			if r.RequestURI != "/health/live" && r.RequestURI != "/health/ready" {
				server.logger.Info().
					Int64("latency", stop.Sub(start).Microseconds()).
					Str("ip", r.RemoteAddr).
					Str("uri", r.RequestURI).
					Str("method", r.Method).
					Msg("request completed")
			}
		})
	})

	// GET /health/live
	r.HandleFunc("/health/live", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}).Methods("GET")

	// GET /health/ready
	r.HandleFunc("/health/ready", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}).Methods("GET")

	// GET /api/manifests/{id}
	r.HandleFunc("/api/manifests/{id}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var m *manifest.Manifest
		if vars["id"] == "self" {
			m = store.GetByIP(r.RemoteAddr)
		} else {
			if authorization != r.Header.Get("Authorization") {
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}
			m = store.GetByID(vars["id"])
		}
		if m == nil {
			http.Error(w, "not found", http.StatusNotFound)
			return
		}
		var b []byte
		if strings.Contains(r.Header.Get("Accept"), "application/json") {
			w.Header().Set("Content-Type", "application/json")
			b, _ = json.Marshal(m)
		} else {
			w.Header().Set("Content-Type", "application/yaml")
			b, _ = yaml.Marshal(m)
		}
		w.WriteHeader(http.StatusOK)
		w.Write(b)
	}).Methods("GET")

	// GET /api/manifests
	r.HandleFunc("/api/manifests", func(w http.ResponseWriter, r *http.Request) {
		if authorization != r.Header.Get("Authorization") {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		var m *manifest.Manifest
		manifests := make(map[string]*manifest.Manifest)
		ip := r.URL.Query().Get("ip")
		mac := r.URL.Query().Get("mac")
		if ip != "" {
			m = store.GetByIP(ip)
			if m == nil {
				http.Error(w, "not found", http.StatusNotFound)
				return
			}
		} else if mac != "" {
			m = store.GetByMAC(mac)
			if m == nil {
				if dynamic, err := strconv.ParseBool(r.URL.Query().Get("dynamic")); err == nil && dynamic {
					m, err = store.GetDynamic(mac)
					if err != nil {
						http.Error(w, fmt.Sprintf("%s", err), http.StatusInternalServerError)
						return
					}
				} else {
					http.Error(w, "not found", http.StatusNotFound)
					return
				}
			}
		} else {
			manifests = store.GetAll()
		}

		var b []byte
		if strings.Contains(r.Header.Get("Accept"), "application/json") {
			w.Header().Set("Content-Type", "application/json")
			if m != nil {
				b, _ = json.Marshal(m)
			} else {
				b, _ = json.Marshal(manifests)
			}
		} else {
			w.Header().Set("Content-Type", "application/yaml")
			if m != nil {
				b, _ = yaml.Marshal(m)
			} else {
				b, _ = yaml.Marshal(manifests)
			}
		}
		w.WriteHeader(http.StatusOK)
		w.Write(b)
	}).Methods("GET")

	// PUT /api/manifests/{id}
	r.HandleFunc("/api/manifests/{id}", func(w http.ResponseWriter, r *http.Request) {
		if authorization != r.Header.Get("Authorization") {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		buf, _ := ioutil.ReadAll(r.Body)
		var m manifest.Manifest
		if r.Header.Get("Content-Type") == "application/json" {
			err = json.Unmarshal(buf, &m)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
		} else {
			m, err = manifest.ManifestFromYaml(buf)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
		}
		_ = store.PutManifest(m)
		w.WriteHeader(http.StatusCreated)
	}).Methods("PUT")

	// DELETE /api/manifests/{id}
	r.HandleFunc("/api/manifests/{id}", func(w http.ResponseWriter, r *http.Request) {
		if authorization != r.Header.Get("Authorization") {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		vars := mux.Vars(r)
		m := store.GetByID(vars["id"])
		if m != nil {
			store.DeleteManifest(m)
		}

		w.WriteHeader(http.StatusNoContent)
	}).Methods("DELETE")

	// GET|POST /api/manifests/{id}/suspend-boot
	r.HandleFunc("/api/manifests/{id}/suspend-boot", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var m *manifest.Manifest
		if vars["id"] == "self" {
			m = store.GetByIP(r.RemoteAddr)
		} else {
			if authorization != r.Header.Get("Authorization") {
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}
			m = store.GetByID(vars["id"])
		}
		if m == nil {
			http.Error(w, "not found", http.StatusNotFound)
			return
		}
		store.Suspend(m.ID)

		w.WriteHeader(http.StatusOK)
	}).Methods("GET", "POST")

	// GET|POST /api/manifests/{id}/unsuspend-boot
	r.HandleFunc("/api/manifests/{id}/unsuspend-boot", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var m *manifest.Manifest
		if vars["id"] == "self" {
			m = store.GetByIP(r.RemoteAddr)
		} else {
			if authorization != r.Header.Get("Authorization") {
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}
			m = store.GetByID(vars["id"])
		}
		if m == nil {
			http.Error(w, "not found", http.StatusNotFound)
			return
		}
		store.Unsuspend(m.ID)

		w.WriteHeader(http.StatusOK)
	}).Methods("GET", "POST")

	// GET|POST /api/manifests/{id}/update-lease
	r.HandleFunc("/api/manifests/{id}/update-lease", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var m *manifest.Manifest
		if vars["id"] == "self" {
			m = store.GetByIP(r.RemoteAddr)
		} else {
			if authorization != r.Header.Get("Authorization") {
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}
			m = store.GetByID(vars["id"])
		}
		if m == nil {
			http.Error(w, "not found", http.StatusNotFound)
			return
		}
		store.UpdateLease(m.ID)

		w.WriteHeader(http.StatusOK)
	}).Methods("GET", "POST")

	// GET all others
	r.HandleFunc("/{path}", func(w http.ResponseWriter, r *http.Request) {
		server.logger.Info().
			Str("path", r.RequestURI).
			Str("client", r.RemoteAddr).
			Msg("incoming HTTP request")

		m := store.GetByIP(r.RemoteAddr)
		if m == nil {
			server.logger.Info().
				Str("path", r.RequestURI).
				Str("client", r.RemoteAddr).
				Str("manifest_for", r.RemoteAddr).
				Msg("no manifest for client")
			http.Error(w, "no manifest for client: "+r.RemoteAddr, http.StatusNotFound)
			return
		}

		mount, err := m.GetMount(r.URL.Path)
		if err != nil {
			server.logger.Error().
				Err(err).
				Str("path", r.URL.Path).
				Str("client", r.RemoteAddr).
				Str("manifest_for", r.RemoteAddr).
				Msg("cannot find mount")

			http.NotFound(w, r)
			return
		}

		server.logger.Trace().
			Interface("mount", mount).
			Msg("found mount")

		if mount.Content != "" {
			tmpl, err := template.New("").Funcs(sprig.TxtFuncMap()).Parse(mount.Content)
			if err != nil {
				server.logger.Error().
					Err(err).
					Msg("failed to parse content template for mount")
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			buf := new(bytes.Buffer)

			err = tmpl.Execute(buf, manifest.ContentContext{
				RemoteIP: net.ParseIP(r.RemoteAddr),
				HttpBaseUrl: &url.URL{
					Scheme: "http",
					Host:   r.Host,
				},
				Manifest: m,
			})
			if err != nil {
				server.logger.Error().
					Err(err).
					Msg("failed to execute content template for mount")
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			http.ServeContent(w, r, mount.Path, time.Time{}, bytes.NewReader(buf.Bytes()))

			server.logger.Info().
				Err(err).
				Str("path", r.RequestURI).
				Str("client", r.RemoteAddr).
				Str("manifest_for", r.RemoteAddr).
				Msg("transfer finished")
		} else if mount.Proxy != "" {
			d, err := mount.ProxyDirector()
			if err != nil {
				server.logger.Error().
					Err(err).
					Msg("failed to parse proxy URL")
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			rp := httputil.ReverseProxy{
				Director: d,
			}
			rp.ServeHTTP(w, r)
			return
		} else if mount.LocalDir != "" {
			path := filepath.Join(mount.LocalDir, mount.Path)

			if mount.AppendSuffix {
				path = filepath.Join(mount.LocalDir, strings.TrimPrefix(r.URL.Path, mount.Path))
			}

			if !strings.HasPrefix(path, mount.LocalDir) {
				server.logger.Error().
					Err(err).
					Msgf("Requested path is invalid: %q", path)
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}

			f, err := os.Open(path)
			if err != nil {
				server.logger.Error().
					Err(err).
					Msgf("Could not get file from local dir: %q", path)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			stat, err := f.Stat()
			if err != nil {
				server.logger.Error().
					Err(err).
					Msgf("could not stat file: %q", path)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			http.ServeContent(w, r, r.URL.Path, stat.ModTime(), f)
			return
		} else {
			// mount has neither .Path, .Proxy nor .LocalDir defined
			server.logger.Error().
				Str("path", r.RequestURI).
				Str("client", r.RemoteAddr).
				Str("manifest_for", r.RemoteAddr).
				Str("mount", mount.Path).
				Msg("mount is empty")

			http.Error(w, "empty mount", http.StatusInternalServerError)
			return

		}

		return

	}).Methods("GET")

	return server, nil
}

func (server *Server) Serve(l net.Listener) error {
	return server.httpServer.Serve(l)
}

func (server *Server) ServeTLS(l net.Listener, certFile string, keyFile string) error {
	return server.httpServer.ServeTLS(l, certFile, keyFile)
}

func queryFirst(r *http.Request, k string) string {
	keys, ok := r.URL.Query()[k]
	if !ok || len(keys[0]) < 1 {
		return ""
	}
	return keys[0]
}
