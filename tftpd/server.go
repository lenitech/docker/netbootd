package tftpd

import (
	"github.com/pin/tftp/v3"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net"
	"net/http"
)

type Server struct {
	httpClient *http.Client
	tftpServer *tftp.Server
	apiUrl     string
	apiToken   string

	logger zerolog.Logger
}

func NewServer(apiUrl string, apiToken string) (server *Server, err error) {

	server = &Server{
		httpClient: &http.Client{},
		logger:     log.With().Str("service", "tftp").Logger(),
		apiUrl:     apiUrl,
		apiToken:   apiToken,
	}

	return server, nil
}

func (server *Server) Serve(conn *net.UDPConn) {
	server.tftpServer = tftp.NewServer(server.tftpReadHandler, nil)
	err := server.tftpServer.Serve(conn)
	if err != nil {
		server.logger.Error().
			Err(err)
	}
}
