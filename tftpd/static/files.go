package static

import (
	"embed"
	"io/fs"
)

//go:embed ipxe.efi undionly.kpxe ipxe_arm64.efi
var Files embed.FS

func GetFilenames(efs *embed.FS) (files []string) {
	if err := fs.WalkDir(efs, ".", func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			return nil
		}

		files = append(files, path)

		return nil
	}); err != nil {
		return nil
	}

	return files
}
