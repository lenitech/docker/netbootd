package main

import (
	"gitlab.com/lenitech/docker/netbootd/cmd"
)

//go:generate protoc proto/*.proto --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative

func main() {
	cmd.Execute()
}
